

function chart_geral_quilo_dia(){
                
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChart);
  

  function drawChart() {

      var dadosParametro = {
                            nomeFuncao : 'geral_quilo_dia'
                          };

      var jsonData = $.ajax({
          url: "./src/dashboard/dashboard.json.php",
          dataType: "json",
          data: dadosParametro,
          async: false
          }).responseText;


      
      var data = new google.visualization.DataTable(jsonData);


      var options = {         
          title: '',
          hAxis: {
            title: '', gridlines: { color: '#333', count: 10 }, titleTextStyle: { color: "#000", fontName: "sans-serif", fontSize: 5, bold: true }
          },
          vAxis: {
            minValue: 0,
          },
          colors: ['#33CB00', '#9F1F90'],
          pointSize: 10,
          annotations: { stemColor: 'white', textStyle: { fontSize: 12, bold: true } },
          chartArea: { width: '90%', height: '60%', top:'20' },
          legend: { position: 'bottom', alignment: 'center' },
          
          width: '100%'
          
        };

        var chart = new google.visualization.AreaChart(document.getElementById('chart_geral_quilo_dia'));

        
       /* 
        var formatter = new google.visualization.NumberFormat(
        {prefix: '', negativeColor: 'red', negativeParens: true});
        formatter.format(data, 1); // Apply formatter to second column
        formatter.format(data, 2); // Apply formatter to second column
        formatter.format(data, 3); // Apply formatter to second column
        */
        chart.draw(data, options);

        /*  VISIVEL E INVISIVEL */
        var columns = [];
        var series = {};
        for (var i = 0; i < data.getNumberOfColumns(); i++) {
          columns.push(i);
          if (i > 0) {
            series[i - 1] = {};
          }
        }

        google.visualization.events.addListener(chart, 'select', function () {
          var sel = chart.getSelection();
          // if selection length is 0, we deselected an element
          if (sel.length > 0) {
            // if row is undefined, we clicked on the legend
            if (sel[0].row === null) {
              var col = sel[0].column;
              if (columns[col] == col) {
                // hide the data series
                columns[col] = {
                  label: data.getColumnLabel(col),
                  type: data.getColumnType(col),
                  calc: function () {
                    return null;
                  }
                };
                // grey out the legend entry
                series[col - 1].color = '#CCCCCC';
              }
              else {
                // show the data series
                columns[col] = col;
                series[col - 1].color = null;
              }
              var view = new google.visualization.DataView(data);
              view.setColumns(columns);
              chart.draw(view, options);
            }
          }
        });

        /*  VISIVEL E INVISIVEL */
  }

  $(window).resize(function () {
    drawChart();
  });

}

function chart_geral_quilo_percentual(){
                
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChart);
  

  function drawChart() {

      var dadosParametro = {
                            nomeFuncao : 'geral_quilo_percentual'
                          };

      var jsonData = $.ajax({
          url: "./src/dashboard/dashboard.json.php",
          dataType: "json",
          data: dadosParametro,
          async: false
          }).responseText;


      
      var data = new google.visualization.DataTable(jsonData);


      var options = {         
          title: '',
          hAxis: {
            title: '', gridlines: { color: '#333', count: 10 }, titleTextStyle: { color: "#000", fontName: "sans-serif", fontSize: 5, bold: true }
          },
          vAxis: {
            minValue: 0,
          },
          colors: ['#33CB00', '#9F1F90'],
          pointSize: 10,
          annotations: { stemColor: 'white', textStyle: { fontSize: 12, bold: true } },
          chartArea: { width: '90%', height: '60%', top:'20' },
          legend: { position: 'bottom', alignment: 'center' },
          
          width: '100%'
          
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart_geral_quilo_percentual'));

        
        
        var formatter = new google.visualization.NumberFormat(
        {suffix: ' %', negativeColor: 'red', negativeParens: true});
        formatter.format(data, 1); // Apply formatter to second column
        formatter.format(data, 2); // Apply formatter to second column
        formatter.format(data, 3); // Apply formatter to second column
        formatter.format(data, 4); // Apply formatter to second column
       
        chart.draw(data, options);

        /*  VISIVEL E INVISIVEL */
        var columns = [];
        var series = {};
        for (var i = 0; i < data.getNumberOfColumns(); i++) {
          columns.push(i);
          if (i > 0) {
            series[i - 1] = {};
          }
        }

        google.visualization.events.addListener(chart, 'select', function () {
          var sel = chart.getSelection();
          // if selection length is 0, we deselected an element
          if (sel.length > 0) {
            // if row is undefined, we clicked on the legend
            if (sel[0].row === null) {
              var col = sel[0].column;
              if (columns[col] == col) {
                // hide the data series
                columns[col] = {
                  label: data.getColumnLabel(col),
                  type: data.getColumnType(col),
                  calc: function () {
                    return null;
                  }
                };
                // grey out the legend entry
                series[col - 1].color = '#CCCCCC';
              }
              else {
                // show the data series
                columns[col] = col;
                series[col - 1].color = null;
              }
              var view = new google.visualization.DataView(data);
              view.setColumns(columns);
              chart.draw(view, options);
            }
          }
        });

        /*  VISIVEL E INVISIVEL */
  }

  $(window).resize(function () {
    drawChart();
  });

}

function chart_geral_pessoas(){
                
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChart);
  

  function drawChart() {

      var dadosParametro = {
                            nomeFuncao : 'geral_pessoa'
                          };

      var jsonData = $.ajax({
          url: "./src/dashboard/dashboard.json.php",
          dataType: "json",
          data: dadosParametro,
          async: false
          }).responseText;


      
      var data = new google.visualization.DataTable(jsonData);


      var options = {         
          title: '',
          hAxis: {
            title: '', gridlines: { color: '#333', count: 10 }, titleTextStyle: { color: "#000", fontName: "sans-serif", fontSize: 5, bold: true }
          },
          vAxis: {
            minValue: 0,
          },
          colors: ['#33CB00', '#9F1F90'],
          pointSize: 10,
          annotations: { stemColor: 'white', textStyle: { fontSize: 12, bold: true } },
          chartArea: { width: '90%', height: '60%', top:'20' },
          legend: { position: 'bottom', alignment: 'center' },
          
          width: '100%'
          
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart_geral_pessoas'));

        
       /* 
        var formatter = new google.visualization.NumberFormat(
        {prefix: '', negativeColor: 'red', negativeParens: true});
        formatter.format(data, 1); // Apply formatter to second column
        formatter.format(data, 2); // Apply formatter to second column
        formatter.format(data, 3); // Apply formatter to second column
        */
        chart.draw(data, options);

        /*  VISIVEL E INVISIVEL */
        var columns = [];
        var series = {};
        for (var i = 0; i < data.getNumberOfColumns(); i++) {
          columns.push(i);
          if (i > 0) {
            series[i - 1] = {};
          }
        }

        google.visualization.events.addListener(chart, 'select', function () {
          var sel = chart.getSelection();
          // if selection length is 0, we deselected an element
          if (sel.length > 0) {
            // if row is undefined, we clicked on the legend
            if (sel[0].row === null) {
              var col = sel[0].column;
              if (columns[col] == col) {
                // hide the data series
                columns[col] = {
                  label: data.getColumnLabel(col),
                  type: data.getColumnType(col),
                  calc: function () {
                    return null;
                  }
                };
                // grey out the legend entry
                series[col - 1].color = '#CCCCCC';
              }
              else {
                // show the data series
                columns[col] = col;
                series[col - 1].color = null;
              }
              var view = new google.visualization.DataView(data);
              view.setColumns(columns);
              chart.draw(view, options);
            }
          }
        });

        /*  VISIVEL E INVISIVEL */
  }

  $(window).resize(function () {
    drawChart();
  });

}

function chart_geral_doacoes(){
                
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChart);
  

  function drawChart() {

      var dadosParametro = {
                            nomeFuncao : 'geral_doacoes'
                          };

      var jsonData = $.ajax({
          url: "./src/dashboard/dashboard.json.php",
          dataType: "json",
          data: dadosParametro,
          async: false
          }).responseText;


      
      var data = new google.visualization.DataTable(jsonData);


      var options = {         
          title: '',
          hAxis: {
            title: '', gridlines: { color: '#333', count: 10 }, titleTextStyle: { color: "#000", fontName: "sans-serif", fontSize: 5, bold: true }
          },
          vAxis: {
            minValue: 0,
          },
          //colors: ['#57c3e6', '#73b958', '#808080'],
          pointSize: 10,
          annotations: { stemColor: 'white', textStyle: { fontSize: 12, bold: true } },
          chartArea: { width: '90%', height: '60%', top:'20' },
          legend: { position: 'bottom', alignment: 'center' },
          
          width: '100%'
          
        };

        var chart = new google.visualization.AreaChart(document.getElementById('chart_geral_doacoes'));
        chart.draw(data, options);
        
       /* 
        var formatter = new google.visualization.NumberFormat(
        {prefix: '', negativeColor: 'red', negativeParens: true});
        formatter.format(data, 1); // Apply formatter to second column
        formatter.format(data, 2); // Apply formatter to second column
        formatter.format(data, 3); // Apply formatter to second column
        */
        
        /*  VISIVEL E INVISIVEL */
        var columns = [];
        var series = {};
        for (var i = 0; i < data.getNumberOfColumns(); i++) {
          columns.push(i);
          if (i > 0) {
            series[i - 1] = {};
          }
        }

        google.visualization.events.addListener(chart, 'select', function () {
          var sel = chart.getSelection();
          // if selection length is 0, we deselected an element
          if (sel.length > 0) {
            // if row is undefined, we clicked on the legend
            if (sel[0].row === null) {
              var col = sel[0].column;
              if (columns[col] == col) {
                // hide the data series
                columns[col] = {
                  label: data.getColumnLabel(col),
                  type: data.getColumnType(col),
                  calc: function () {
                    return null;
                  }
                };
                // grey out the legend entry
                series[col - 1].color = '#CCCCCC';
              }
              else {
                // show the data series
                columns[col] = col;
                series[col - 1].color = null;
              }
              var view = new google.visualization.DataView(data);
              view.setColumns(columns);
              chart.draw(view, options);
            }
          }
        });

        /*  VISIVEL E INVISIVEL */
  }

  $(window).resize(function () {
    drawChart();
  });

}

function chart_genero(){
                
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChart);
  

  function drawChart() {

      var dadosParametro = {
                            nomeFuncao : 'genero'
                          };

      var jsonData = $.ajax({
          url: "./src/dashboard/dashboard.json.php",
          dataType: "json",
          data: dadosParametro,
          async: false
          }).responseText;


      
      var data = new google.visualization.arrayToDataTable(JSON.parse(jsonData));


      var options = {           
        
        legend: { position: 'bottom', alignment: 'center' },        
        width: '100%'
        };

        var chart = new google.visualization.PieChart(document.getElementById('chart_genero'));

        
        
       /* var formatter = new google.visualization.NumberFormat(
        {prefix: '', negativeColor: 'red', negativeParens: true});
        formatter.format(data, 1); // Apply formatter to second column
        formatter.format(data, 2); // Apply formatter to second column
        formatter.format(data, 3); // Apply formatter to second column
        */
        chart.draw(data, options);

      /*  VISIVEL E INVISIVEL */
      var columns = [];
      var series = {};
      for (var i = 0; i < data.getNumberOfColumns(); i++) {
        columns.push(i);
        if (i > 0) {
          series[i - 1] = {};
        }
      }

      google.visualization.events.addListener(chart, 'select', function () {
        var sel = chart.getSelection();
        // if selection length is 0, we deselected an element
        if (sel.length > 0) {
          // if row is undefined, we clicked on the legend
          if (sel[0].row === null) {
            var col = sel[0].column;
            if (columns[col] == col) {
              // hide the data series
              columns[col] = {
                label: data.getColumnLabel(col),
                type: data.getColumnType(col),
                calc: function () {
                  return null;
                }
              };
              // grey out the legend entry
              series[col - 1].color = '#CCCCCC';
            }
            else {
              // show the data series
              columns[col] = col;
              series[col - 1].color = null;
            }
            var view = new google.visualization.DataView(data);
            view.setColumns(columns);
            chart.draw(view, options);
          }
        }
      });

      /*  VISIVEL E INVISIVEL */
  }

  $(window).resize(function () {
    drawChart();
  });

}
 
function chart_turno(){
                
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChart);
  

  function drawChart() {

      var dadosParametro = {
                            nomeFuncao : 'turno'
                          };

      var jsonData = $.ajax({
          url: "./src/dashboard/dashboard.json.php",
          dataType: "json",
          data: dadosParametro,
          async: false
          }).responseText;


      
      var data = new google.visualization.arrayToDataTable(JSON.parse(jsonData));


      var options = {           
        
        legend: { position: 'bottom', alignment: 'center' },        
        width: '100%'
        };

        var chart = new google.visualization.PieChart(document.getElementById('chart_turno'));

        
        
       /* var formatter = new google.visualization.NumberFormat(
        {prefix: '', negativeColor: 'red', negativeParens: true});
        formatter.format(data, 1); // Apply formatter to second column
        formatter.format(data, 2); // Apply formatter to second column
        formatter.format(data, 3); // Apply formatter to second column
        */
        chart.draw(data, options);

      /*  VISIVEL E INVISIVEL */
      var columns = [];
      var series = {};
      for (var i = 0; i < data.getNumberOfColumns(); i++) {
        columns.push(i);
        if (i > 0) {
          series[i - 1] = {};
        }
      }

      google.visualization.events.addListener(chart, 'select', function () {
        var sel = chart.getSelection();
        // if selection length is 0, we deselected an element
        if (sel.length > 0) {
          // if row is undefined, we clicked on the legend
          if (sel[0].row === null) {
            var col = sel[0].column;
            if (columns[col] == col) {
              // hide the data series
              columns[col] = {
                label: data.getColumnLabel(col),
                type: data.getColumnType(col),
                calc: function () {
                  return null;
                }
              };
              // grey out the legend entry
              series[col - 1].color = '#CCCCCC';
            }
            else {
              // show the data series
              columns[col] = col;
              series[col - 1].color = null;
            }
            var view = new google.visualization.DataView(data);
            view.setColumns(columns);
            chart.draw(view, options);
          }
        }
      });

      /*  VISIVEL E INVISIVEL */
  }

  $(window).resize(function () {
    drawChart();
  });

}

function chart_turno_trocados(){
                
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChart);
  

  function drawChart() {

      var dadosParametro = {
                            nomeFuncao : 'turno_trocados'
                          };

      var jsonData = $.ajax({
          url: "./src/dashboard/dashboard.json.php",
          dataType: "json",
          data: dadosParametro,
          async: false
          }).responseText;


      
      var data = new google.visualization.arrayToDataTable(JSON.parse(jsonData));


      var options = {           
        
        legend: { position: 'bottom', alignment: 'center' },        
        width: '100%'
        };

        var chart = new google.visualization.PieChart(document.getElementById('chart_turno_trocados'));

        
        
       /* var formatter = new google.visualization.NumberFormat(
        {prefix: '', negativeColor: 'red', negativeParens: true});
        formatter.format(data, 1); // Apply formatter to second column
        formatter.format(data, 2); // Apply formatter to second column
        formatter.format(data, 3); // Apply formatter to second column
        */
        chart.draw(data, options);

      /*  VISIVEL E INVISIVEL */
      var columns = [];
      var series = {};
      for (var i = 0; i < data.getNumberOfColumns(); i++) {
        columns.push(i);
        if (i > 0) {
          series[i - 1] = {};
        }
      }

      google.visualization.events.addListener(chart, 'select', function () {
        var sel = chart.getSelection();
        // if selection length is 0, we deselected an element
        if (sel.length > 0) {
          // if row is undefined, we clicked on the legend
          if (sel[0].row === null) {
            var col = sel[0].column;
            if (columns[col] == col) {
              // hide the data series
              columns[col] = {
                label: data.getColumnLabel(col),
                type: data.getColumnType(col),
                calc: function () {
                  return null;
                }
              };
              // grey out the legend entry
              series[col - 1].color = '#CCCCCC';
            }
            else {
              // show the data series
              columns[col] = col;
              series[col - 1].color = null;
            }
            var view = new google.visualization.DataView(data);
            view.setColumns(columns);
            chart.draw(view, options);
          }
        }
      });

      /*  VISIVEL E INVISIVEL */
  }

  $(window).resize(function () {
    drawChart();
  });

}

function chart_geral_quilo_(){
                
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChart);
  

  function drawChart() {

      var dadosParametro = {
                            nomeFuncao : 'geral_quilo'
                          };

      var jsonData = $.ajax({
          url: "./src/dashboard/dashboard.json.php",
          dataType: "json",
          data: dadosParametro,
          async: false
          }).responseText;


      
      var data = new google.visualization.arrayToDataTable(JSON.parse(jsonData));


      var options = {           
        
        legend: { position: 'bottom', alignment: 'center' },        
        width: '100%',
        colors: ['#33CB00', '#9F1F90'],

        };

        var chart = new google.visualization.PieChart(document.getElementById('chart_geral_quilo'));

        
        
       /* var formatter = new google.visualization.NumberFormat(
        {prefix: '', negativeColor: 'red', negativeParens: true});
        formatter.format(data, 1); // Apply formatter to second column
        formatter.format(data, 2); // Apply formatter to second column
        formatter.format(data, 3); // Apply formatter to second column
        */
        chart.draw(data, options);

      /*  VISIVEL E INVISIVEL */
      var columns = [];
      var series = {};
      for (var i = 0; i < data.getNumberOfColumns(); i++) {
        columns.push(i);
        if (i > 0) {
          series[i - 1] = {};
        }
      }

      google.visualization.events.addListener(chart, 'select', function () {
        var sel = chart.getSelection();
        // if selection length is 0, we deselected an element
        if (sel.length > 0) {
          // if row is undefined, we clicked on the legend
          if (sel[0].row === null) {
            var col = sel[0].column;
            if (columns[col] == col) {
              // hide the data series
              columns[col] = {
                label: data.getColumnLabel(col),
                type: data.getColumnType(col),
                calc: function () {
                  return null;
                }
              };
              // grey out the legend entry
              series[col - 1].color = '#CCCCCC';
            }
            else {
              // show the data series
              columns[col] = col;
              series[col - 1].color = null;
            }
            var view = new google.visualization.DataView(data);
            view.setColumns(columns);
            chart.draw(view, options);
          }
        }
      });

      /*  VISIVEL E INVISIVEL */
  }

  $(window).resize(function () {
    drawChart();
  });

}

function chart_geral_quilo(){             
  

  $('#lottie1 div').empty();  
  var animation1 = bodymovin.loadAnimation({
    container: document.getElementById('lottie1'), // Required
    path: './funcoes/loader.json', 
    renderer: 'svg', 
    loop: true, 
    autoplay: true, 
    name: 'Trocados', 
  })

  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChart);
  

  function drawChart() {

      var dadosParametro = {
                            nomeFuncao : 'geral_quilo'
                          };


      $.ajax({
          url: "./src/dashboard/dashboard.json.php",
          dataType: "json",
          data: dadosParametro,
          async: false,
          success: function (jsonData) {              
            var data = new google.visualization.arrayToDataTable(jsonData);

            var options = {           
              
              legend: { position: 'bottom', alignment: 'center' },        
              width: '100%',
              colors: ['#33CB00', '#9F1F90'],

              };

              var chart = new google.visualization.PieChart(document.getElementById('chart_geral_quilo'));

              
              
            /* var formatter = new google.visualization.NumberFormat(
              {prefix: '', negativeColor: 'red', negativeParens: true});
              formatter.format(data, 1); // Apply formatter to second column
              formatter.format(data, 2); // Apply formatter to second column
              formatter.format(data, 3); // Apply formatter to second column
              */
              chart.draw(data, options);

            /*  VISIVEL E INVISIVEL */
            var columns = [];
            var series = {};
            for (var i = 0; i < data.getNumberOfColumns(); i++) {
              columns.push(i);
              if (i > 0) {
                series[i - 1] = {};
              }
            }

            google.visualization.events.addListener(chart, 'select', function () {
              var sel = chart.getSelection();
              // if selection length is 0, we deselected an element
              if (sel.length > 0) {
                // if row is undefined, we clicked on the legend
                if (sel[0].row === null) {
                  var col = sel[0].column;
                  if (columns[col] == col) {
                    // hide the data series
                    columns[col] = {
                      label: data.getColumnLabel(col),
                      type: data.getColumnType(col),
                      calc: function () {
                        return null;
                      }
                    };
                    // grey out the legend entry
                    series[col - 1].color = '#CCCCCC';
                  }
                  else {
                    // show the data series
                    columns[col] = col;
                    series[col - 1].color = null;
                  }
                  var view = new google.visualization.DataView(data);
                  view.setColumns(columns);
                  chart.draw(view, options);
                }
              }
            });

            /*  VISIVEL E INVISIVEL */                            
          },
          beforeSend: function (dados, status, jqXHR) {
            $(".overlay").css('display', 'block');
          },
          complete: function () {
            $(".overlay").css('display', 'none');
          }
  
      })
  }

  $(window).resize(function () {
    drawChart();
  });

}

function verificar_atualizacao(){ 

  var totalQuilogramaAtual = 0;
  var totalQuilogramaExternoAtual = 0;
  var tempoAtualizar = 18;

  function consultaQuilo() {
    var dadosParametro = {
        nomeFuncao : 'geral'
      };
      
      $.ajax({
      url: './src/dashboard/dashboard.json.php',
        dataType: "json",
        data: dadosParametro,
        success: function (response) {              
          if ((response[0].totalQuilograma != totalQuilogramaAtual) || ((response[0].totalQuilogramaExterno != totalQuilogramaExternoAtual))) {
            //
            $('#notificacao').trigger('play');

            totalQuilogramaAtual = response[0].totalQuilograma;
            totalQuilogramaExternoAtual = response[0].totalQuilogramaExterno;
            //
            document.getElementById('totalQuantidadeDoacao').innerHTML = response[0].totalQuantidadeDoacao;
            document.getElementById('totalQuilograma').innerHTML = response[0].totalQuilograma + ' kg';
            document.getElementById('totalQuilogramaExterno').innerHTML = response[0].totalQuilogramaExterno + ' kg';
            document.getElementById('idadeMedia').innerHTML = response[0].idadeMedia;
            document.getElementById('totalCancelamento').innerHTML = response[0].totalCancelamento;
            document.getElementById('totalPessoas').innerHTML = response[0].totalPessoas;            

            chart_geral_quilo_dia();  
            chart_geral_quilo_percentual();  
            chart_geral_doacoes();
            chart_geral_pessoas();
            chart_genero();    
            chart_geral_quilo();     
            chart_turno();   
            chart_turno_trocados()
          }
           
          
        }, 
        complete: function () { 
          setTimeout(function() {consultaQuilo()}, tempoAtualizar * 1000);
        } 

    })
  }
  
  consultaQuilo();

}





