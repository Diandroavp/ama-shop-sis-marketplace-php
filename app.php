<div class="m-3">
<?php

    include "src/geral/login/login.sessions.php";
	//
	$_p = (isset($_REQUEST['_p'])) ? $_REQUEST['_p'] : null;


	if ($_SESSION["_SESSION_tipoAcesso"] <> '') {		
		switch($_p) {
			/*	GERAL	*/
			case "sai": /*Sair*/
				include "src/geral/login/login.processo.sair.php";
				//
				break;
			/*	CADASTRO	*/
			case "usu": /*Operador*/
				include "src/cadastro/operador/index.php";
				//
				break;
			case "doa": /*Doações Extenras*/
				include "src/cadastro/doacaoexterna/index.php";
				//
				break;	
			/*	ESTOQUE	*/		
			case "prod": /*contato por produto*/
				include "src/estoque/produto/index.php";
				//
				break;
			case "pimg": /*contato por produto*/
				include "src/estoque/produtoImagem/index.php";
				//
				break;
			/*	MOVIMENTO	*/
			case "vali": /*validar Doação realizadas*/
				include "src/movimento/validar/index.php";
				//
				break;
			case "hisv": /*Histórico de Validação*/
				include "src/movimento/historico/index.php";
				//
				break;			
			case "doac": /*Compra*/
				include "src/movimento/compra/index.php";
				//
				break;
			/*	DASHBOARD	*/
			case "dash": /*Dashboard Principal*/
				include "src/dashboard/index.php";
				//
				break;
			default:
					include "inicio.view.php";   
				break;
		}
		
	} else {        
		include "src/geral/login/index.php";    
	}

?>
</div>
