<?php

/************************Login***************************/
    // Login
    function login($Operador, $senha ){
        $connection = abrirConnection();  
        $params = array($Operador, $senha);
        $dados = executeSP("spOperador_Autenticar", $params, $connection);
        return $dados;
    }
    

/******************Cadastro**************************/	

function operador($tipoAcao, $idOperador, $nomeOperador, $eMail, $senhaOperador, $ativoOperador, $tipoNivel){
    $connection = abrirConnection();   
    /*
    $tipoAcao = 's';
    $idOperador = null;
    $nomeOperador = null;
    $eMail  = null;
    $senhaOperador   = null;
    $ativoOperador   = 0;
    */
    $params = array($tipoAcao, $idOperador, $nomeOperador, $eMail, $senhaOperador, $ativoOperador, $tipoNivel);        
    $dados = executeSP("spOperador", $params, $connection);
    return $dados;
}

function doacaoExterna($tipoAcao, $idDoacaoExterna, $numeroDoacoes, $quantidadeQuilos, $dataEvento){
    $connection = abrirConnection();   
    $params = array($tipoAcao, $idDoacaoExterna, $numeroDoacoes, $quantidadeQuilos, $dataEvento);        
    $dados = executeSP("spDoacaoExterna", $params, $connection);
    return $dados;
}

/******************Estoque**************************/	

function produto($tipoAcao, $idProduto, $nomeProduto, $tipoProdutoPesquisa, $valorProduto, $valorOriginal, $nomeImagem, $nomeImagemSecundaria, $percentualDesconto, $idParceiro, $textoDescricao, $ativoMostrarTelaPrincipal, $tipoProduto, $ativoProduto, $quantidadeDisponivel, $tipoGiftCard, $nomeOperadoraRecarga, $ativoControleCodigo, $textoInstrucao, $ativoEventoNatal, $quantidadeQuilograma){
    $connection = abrirConnection();   
    $params = array($tipoAcao, $idProduto, $nomeProduto, $tipoProdutoPesquisa, $valorProduto, $valorOriginal, $nomeImagem, $nomeImagemSecundaria, $percentualDesconto, $idParceiro, $textoDescricao, $ativoMostrarTelaPrincipal, $tipoProduto, $ativoProduto, $quantidadeDisponivel, $tipoGiftCard, $nomeOperadoraRecarga, $ativoControleCodigo, $textoInstrucao, $ativoEventoNatal, $quantidadeQuilograma);
    $dados = executeSP("spProduto", $params, $connection);
    return $dados;
}

function parceirosSelecionar($idParceiros){
    $connection = abrirConnection();   
    $params = array($idParceiros);
    $dados = executeSP("spParceiros_Selecionar", $params, $connection);
    return $dados;
}

function produtoImagem($tipoAcao, $idImagem, $idProduto, $nomeImagem){
    $connection = abrirConnection();   
    $params = array($tipoAcao, $idImagem, $idProduto, $nomeImagem);
    $dados = executeSP("spProdutoImagem", $params, $connection);
    return $dados;
}

/******************Movimento**************************/	


function compraValidar($tipoAcao, $nomeUsuario, $numeroCelular, $numeroCPF, $idCompra, $idOperador, $textoObservacao, $ativoProducao, $idCompraValidacao){
    $connection = abrirConnection();   
    $params = array($tipoAcao, $nomeUsuario, $numeroCelular, $numeroCPF, $idCompra, $idOperador, $textoObservacao, $ativoProducao, $idCompraValidacao);   
    $dados = executeSP("spCompra_Validar", $params, $connection);
    return $dados;
}

function compraValidacaoHistorico($tipoAcao, $nomeUsuario, $numeroCelular, $numeroCPF, $idCompra, $idOperador, $dataInicio, $dataFinal, $ativoProducao, $idCompraValidacao){
    $connection = abrirConnection();  
    $params = array($tipoAcao, $nomeUsuario, $numeroCelular, $numeroCPF, $idCompra, $idOperador, $dataInicio, $dataFinal, $ativoProducao, $idCompraValidacao);   
    $dados = executeSP("spCompraValidacao_Historico", $params, $connection);
    return $dados;
}

function compra($tipoAcao, $nomeUsuario, $numeroCelular, $numeroCPF, $idCompra, $idOperador, $dataInicio, $dataFinal, $ativoProducao, $idCompraValidacao, $textoEstorno){
    $connection = abrirConnection();   
    $params = array($tipoAcao, $nomeUsuario, $numeroCelular, $numeroCPF, $idCompra, $idOperador, $dataInicio, $dataFinal, $ativoProducao, $idCompraValidacao, $textoEstorno);   
    $dados = executeSP("spCompra", $params, $connection);
    return $dados;
}


/******************Dashboard**************************/	


function dashboardGeral(){
    $connection = abrirConnection();   
    $params = array();   
    $dados = executeSP("spDashboard_Geral", $params, $connection);
    return $dados;
}

function dashboardQuantidade($dataAcao){
    $connection = abrirConnection();   
    $params = array($dataAcao);   
    $dados = executeSP("spDashboard_Quantidade", $params, $connection);
    return $dados;
}

function dashboardGenero($ativoProducao){
    $connection = abrirConnection();   
    $params = array( $ativoProducao);   
    $dados = executeSP("spDashboard_Genero", $params, $connection);
    return $dados;
}

function dashboardPessoa($dataAcao){
    $connection = abrirConnection();   
    $params = array($dataAcao);   
    $dados = executeSP("spDashboard_Pessoas", $params, $connection);
    return $dados;
}

function dashboardTurno($tipoTurno){
    $connection = abrirConnection();   
    $params = array($tipoTurno);   
    $dados = executeSP("spDashboard_Turno", $params, $connection);
    return $dados;
}




?>