
<!-- Esse trecho é importante para colocar os IDs e Nomes para os Inputs -->
<div class="hide">
    <?php foreach($dadosOperador as $item) {?>	
        <form role="form" id="modalExcluir<?=$item['idOperador']?>" action="<?=$textoDirecionar?>" method="post">
          <input type="hidden" name="tipoAcao" id="tipoAcao" value="D" />
          <input type="hidden" name="tipoPesquisa"  value="<?=$tipoPesquisa?>" />
          <input type="hidden" name="textoPesquisa"  value="<?=$textoPesquisa?>" />
          <input type="hidden" name="idOperador" id="idOperador" value="<?=$item['idOperador']?>" />            
        </form>
    <?php  }?>
</div>

<script type="text/JavaScript">
    function enviaform(texto, codigo){
          var excluir = confirm(texto);
            
          if (excluir) {
            document.getElementById('modalExcluir' + codigo).submit();           
          } else {
            return false;
          }
        }
</script>