<?php	
    $textoDirecionar = "?_p=".$_REQUEST["_p"];	
	$idOperadorCorrente = $_SESSION["_SESSION_idOperador"];
	// Tipo de Pesquisa
	$tipoPesquisa = (isset($_REQUEST['tipoPesquisa'])) ? $_REQUEST['tipoPesquisa'] : null;
	$textoPesquisa = (isset($_REQUEST['textoPesquisa'])) ? $_REQUEST['textoPesquisa'] : null;
	// CRUD
	$tipoAcao = (isset($_REQUEST['tipoAcao'])) ? $_REQUEST['tipoAcao'] : 'S';
	$idOperador = (isset($_REQUEST['idOperador'])) ? $_REQUEST['idOperador'] : null;  
	$nomeOperador = (isset($_REQUEST['nomeOperador'])) ? $_REQUEST['nomeOperador'] : null;  
	$eMail = (isset($_REQUEST['eMail'])) ? $_REQUEST['eMail'] : null;  
	$senhaOperador = (isset($_REQUEST['senhaOperador'])) ? $_REQUEST['senhaOperador'] : null;  
	$tipoNivel = (isset($_REQUEST['tipoNivel'])) ? $_REQUEST['tipoNivel'] : null;  
	$ativoOperador = (isset($_REQUEST['ativoOperador'])) ? $_REQUEST['ativoOperador'] : 0;   
	// Retorno de Resposta
	$tipoResposta = (isset($_REQUEST['tipoResposta'])) ? $_REQUEST['tipoResposta'] : null;
	$textoRetorno = (isset($_REQUEST['textoRetorno'])) ? $_REQUEST['textoRetorno'] : null;
	$ativoRetorno = (isset($_REQUEST['ativoRetorno'])) ? $_REQUEST['ativoRetorno'] : null;	
	

    if (isset($_REQUEST['tipoAcao']) && ($tipoAcao != 'S')){
		
		$dadosResposta = operador($tipoAcao, 
								$idOperador, 
								$nomeOperador, 
								$eMail, 
								$senhaOperador, 
								$ativoOperador,
								$tipoNivel );
								
		if ($dadosResposta and ($dadosResposta[0]['ativoRetorno'])){
			//AUDITORIA
			include "operador.auditoria.php";
		}
		//
		include "operador.processo.resposta.php";
    }
	if (isset($_REQUEST['tipoResposta'])){
		showMessage(($ativoRetorno == 1) ? "S" : "E", $_CONFIGURACAO_TITULO." - Informativo", $textoRetorno);
	}	

	// Montando tipo de Pesquisa
	$paramPesquisa = array();
	$paramPesquisa[] = array('idOperador', 'Código');
	$paramPesquisa[] = array('nomeOperador', 'Operador');
	$paramPesquisa[] = array('eMail', 'e-Mail');

	// 
	if (($tipoAcao == 'S')) {
		// Dados da grid principal
		$dadosOperador = operador($tipoAcao, 
								(($textoPesquisa != '') && ($tipoPesquisa == 'idOperador')) ? $textoPesquisa : null, 
								(($textoPesquisa != '') && ($tipoPesquisa == 'nomeOperador')) ? $textoPesquisa : null, 
								(($textoPesquisa != '') && ($tipoPesquisa == 'eMail')) ? $textoPesquisa : null,
								$senhaOperador, 
								$ativoOperador,
								$tipoNivel);
	}
		
    $numeroLinhas = 0;
?>

