<!-- Esse trecho é importante para colocar os IDs e Nomes para os Inputs -->
<?php foreach($dadosDoacaoExterna as $item) {?>
	<div class="modal fade" data-backdrop="static" id="ModeloEditar<?=$item['idDoacaoExterna']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-ms">
			<div class="modal-content">
				<form role="form" name="modalForm" id="modalFormEditar<?=$item['idDoacaoExterna']?>" action="<?=$textoDirecionar?>" method="post">
					<div class="modal-header bg-primary">
						<button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true" class="">×   </span><span class="sr-only">Fechar</span>

						</button>
						<h4 class="modal-title" id="myModalLabel">Editando Registro [<?=$item['idDoacaoExterna']?>]</h4>
					</div>
					<div class="modal-body">
                        <div class="form-group">		
                            <input type="hidden" name="tipoAcao" id="tipoAcao" value="E" />
                            <input type="hidden" name="tipoPesquisa"  value="<?=$tipoPesquisa?>" />
                            <input type="hidden" name="textoPesquisa"  value="<?=$textoPesquisa?>" />
                            <input type="hidden" name="idDoacaoExterna"  value="<?=$item['idDoacaoExterna']?>" />
                        
                            <fieldset>
                                <legend>Dados da Doação</legend>
                                <div class="row">
                                    <div class="col-sm-2">
                                        <label for="Código">Código</label>
                                        <input class="form-control" Disabled  value="<?=$item['idDoacaoExterna']?>" />                               
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label for="dataEvento">Data do Evento</label>
                                        <input class="form-control" type="date" name="dataEvento" id="dataEvento" value="<?=$item['dataEvento']?>" required />
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="numeroDoacoes">Número de Doações</label>
                                        <input class="form-control" type="number" name="numeroDoacoes" id="numeroDoacoes" value="<?=$item['numeroDoacoes']?>" required />
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="quantidadeQuilos">Quilos Doados</label>
                                        <input class="form-control" type="number" name="quantidadeQuilos" id="quantidadeQuilos" value="<?=$item['quantidadeQuilos']?>" required />                               
                                    </div>                                	                                
                                </div>                                
                            </fieldset>
                        </div>
                    </div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
						<button type="button" class="btn btn-success"  onclick="return salvarEnviarForm('modalFormEditar<?=$item['idDoacaoExterna']?>')"  >Salvar Mudanças</button>
					</div>
				</form>
			</div>
		</div>
	</div>

<?php  }?>

<script>
    function salvarEnviarForm(form){              
        document.getElementById(form).submit();           
    }
</script>
