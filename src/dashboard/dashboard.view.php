<div class="container">
  <div class="panel panel-container">
			<div class="row">  

        <div class="col-sm-12 col-md-4 col-lg-2 no-padding">
          <div class="panel panel-default">
            <div class="panel-heading format-trocados">
              Quilos Trocados<em class="fas fa-heart color-blue"></em>
            </div>
            <div class="panel-body">
            <center><h4><div id="totalQuilograma"></div></h4></center>
            </div>
          </div>
				</div>
      
        <div class="col-sm-12 col-md-4 col-lg-2 no-padding">
          <div class="panel panel-default">
            <div class="panel-heading format-personalizado1" >
              Quilos Ama. Shopping <em class="fas fa-heart color-blue"></em>
            </div>
            <div class="panel-body">
            <center><h4><div id="totalQuilogramaExterno"></div></h4></center>
            </div>
          </div>
				</div>      

				<div class="col-sm-6 col-md-3 col-lg-2 no-padding">
          <div class="panel panel-default">
            <div class="panel-heading">
              Quantidade de Doações <em class="fa fa-xl fa-shopping-cart color-blue"></em>
            </div>
            <div class="panel-body ">
              <center><h4><div id="totalQuantidadeDoacao"></div></h4></center>
            </div>
          </div>					
				</div>

      	<div class="col-sm-6 col-md-3 col-lg-2 no-padding">
          <div class="panel panel-default">
            <div class="panel-heading">
              Idade Média <em class="	fas fa-user-friends color-blue"></em>
            </div>
            <div class="panel-body">
            <center><h4><div id="idadeMedia"></div></h4></center>
            </div>
          </div>
				</div>

        <div class="col-sm-6 col-md-3 col-lg-2 no-padding">
          <div class="panel panel-default">
            <div class="panel-heading">
              Total Pessoas <em class="	fas fa-user-friends color-blue"></em>
            </div>
            <div class="panel-body">
            <center><h4><div id="totalPessoas"></div></h4></center>
            </div>
          </div>
				</div>
			
      	<div class="col-sm-6 col-md-3 col-lg-2 no-padding">
          <div class="panel panel-default">
            <div class="panel-heading">
              Total de Cancelamento <em class="fas fa-history color-blue"></em>
            </div>
            <div class="panel-body">
            <center><h4><div id="totalCancelamento"></div></h4></center>
            </div>
        </div>
			
      </div>
		</div>

    

</div>

<!--  GRAFICOS  -->
  <div class="row">      

  <div class="col-sm-6">
    <div class="panel panel-default">
      <div class="panel-heading">Quilos Recebidos Por Dia</div>
        <div class="panel-body"><div id="chart_geral_quilo_dia"></div></div>
    </div>
    </div> 

    <div class="col-sm-6">
    <div class="panel panel-default">
      <div class="panel-heading">Quilos Recebidos Por Dia % </div>
        <div class="panel-body"><div id="chart_geral_quilo_percentual"></div></div>
    </div>
    </div> 
  </div>

  <div class="row">      
  <div class="col-sm-6">
    <div class="panel panel-default">
      <div class="panel-heading">Doações x Validações por dia </div>
        <div class="panel-body"><div id="chart_geral_doacoes"></div></div>
    </div>
    </div> 

    <div class="col-sm-6">
      <div class="panel panel-default">
        <div class="panel-heading">Contagem Pessoas por Dia</div>
          <div class="panel-body"><div id="chart_geral_pessoas"></div></div>
      </div>
    </div>   

  </div> 

  <div class="row">          

    <div class="col-sm-6">
      <div class="panel panel-default">
        <div class="panel-heading">Quilos no Total</div>
          <div class="panel-body">
            <div id="chart_geral_quilo"></div>
          </div>
      </div>
    </div> 

    <div class="col-sm-6">
      <div class="panel panel-default">
        <div class="panel-heading">Quilos por Turno</div>
          <div class="panel-body">
            <div id="chart_turno"></div>
          </div>
      </div>
    </div> 

    <div class="col-sm-6">
      <div class="panel panel-default">
        <div class="panel-heading">Quilos por Turno Trocados</div>
          <div class="panel-body">
            <div id="chart_turno_trocados"></div>
          </div>
      </div>
    </div> 

    <div class="col-sm-6">
      <div class="panel panel-default">
        <div class="panel-heading">Contagem Por Gênero</div>
          <div class="panel-body"><div id="chart_genero"></div></div>
      </div>
    </div> 

  </div> 


</div>

<audio id="notificacao" preload="auto">
  <source src="audio/trocados.mp3" type="audio/mpeg">
</audio>

<script type="text/javascript">           
 verificar_atualizacao();
</script>