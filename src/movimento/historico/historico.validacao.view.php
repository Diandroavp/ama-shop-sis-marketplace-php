<!-- Esse trecho é importante para colocar os IDs e Nomes para os Inputs -->
<?php foreach($dadosCompraValidar as $item) {?>
	<div class="modal fade" data-backdrop="static" id="ModeloValidacao<?=$item['idCompraValidacao']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-ms">
			<div class="modal-content">
                <form role="form" name="modalForm" id="modalFormValidacao<?=$item['idCompraValidacao']?>" action="<?=$textoDirecionar?>" method="post">
					<div class="modal-header bg-primary">
						<button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true" class="">×   </span><span class="sr-only">Fechar</span>

						</button>
						<h4 class="modal-title" id="myModalLabel">Informação de validação de Doação [<?=$item['nome']?> - <?=$item['nomeProduto']?>]</h4>
					</div>
					<div class="modal-body">
                        <div class="form-group">		
                            <input type="hidden" name="tipoAcao" id="tipoAcao" value="D" />
                            <input type="hidden" name="tipoPesquisa"  value="<?=$tipoPesquisa?>" />
                            <input type="hidden" name="textoPesquisa"  value="<?=$textoPesquisa?>" />
                            <input type="hidden" name="idCompraValidacao"  value="<?=$item['idCompraValidacao']?>" />

                            <input type="hidden" name="dataInicio"  value="<?=$dataInicio?>" />
                            <input type="hidden" name="dataFinal"  value="<?=$dataFinal?>" />
                            <input type="hidden" name="idOperador"  value="<?=$idOperador?>" />
                        
                            <fieldset>
                                <legend>Dados da Doação</legend>                                
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label for="textoObservacao">Usuário </label>
                                        <div class="form-group">
                                            <?=$item['nome']?>                                    
                                        </div>
                                    </div> 
                                    <div class="col-sm-4">
                                        <label for="textoObservacao">Usuário </label>
                                        <div class="form-group">
                                            <?=$item['cpf']?>                                    
                                        </div>
                                    </div>  
                                    <div class="col-sm-4">
                                        <label for="textoObservacao">Data da Doação </label>
                                        <div class="form-group">
                                            <?=date_dd_mm_yyyy($item['dataCompra'])?>                                       
                                        </div>                                                                      
                                    </div>                                    
                                </div>  
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label for="textoObservacao">Parceiro </label>
                                        <div class="form-group">
                                            <?=$item['nomeParceiro']?>                                    
                                        </div>
                                    </div> 
                                    <div class="col-sm-8">
                                        <label for="textoObservacao">Produto </label>
                                        <div class="form-group">
                                            <?=$item['nomeProduto']?>                                    
                                        </div>
                                    </div>                                      
                                </div>                                      
                            </fieldset>

                            <fieldset>
                                <legend>Dados da Validação da doação</legend>                                
                                <div class="row">
                                    <div class="col-sm-8">
                                        <label for="textoObservacao">Operador que validou a doação </label>
                                        <div class="form-group">
                                            <?=$item['nomeOperador']?>                                    
                                        </div>
                                    </div>  
                                    <div class="col-sm-4">
                                        <label for="textoObservacao">Data da Validação </label>
                                        <div class="form-group">
                                            <?=date_dd_mm_yyyy($item['dataValidacao'])?>                                       
                                        </div>
                                                                      
                                    </div>                                    
                                </div>  
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label for="textoObservacao">Observação </label>
                                        <textarea class="form-control"  name="textoObservacao" id="textoObservacao" rows="5" maxlength="500" disabled><?=$item['textoObservacao']?></textarea>                              
                                    </div>                                    
                                </div>                                      
                            </fieldset>

                            

                            <fieldset>
                                <legend>Dados para desfazer a validação</legend>                                
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label for="textoJustificativa">Justificativa (Obrigatório) | Máximo de 500 caracteres </label>
                                        <textarea class="form-control"  name="textoJustificativa" id="textoJustificativa<?=$item['idCompraValidacao']?>" rows="5" maxlength="500" required></textarea>                              
                                    </div>                                    
                                </div>                                      
                            </fieldset>

                        </div>
                    </div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-danger" autofocus  onclick="return salvarEnviarForm('modalFormValidacao<?=$item['idCompraValidacao']?>', '<?=$item['idCompraValidacao']?>')"  >Desfazer a validação</button>
					</div>
                </form>
			</div>
		</div>
	</div>

<?php  }?>
<script>
    function salvarEnviarForm(form, idCompraValidacao){  
        let textoJustificativa = document.getElementById('textoJustificativa' + idCompraValidacao);
        
        if ( textoJustificativa.value.length > 15 ){
            document.getElementById(form).submit();           
        } else {
            alert('informe uma justificativa válida! ( mais de 15 caracteres )');
            return false;
        }
        
    }
</script>
