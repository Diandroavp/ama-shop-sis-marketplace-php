<!-- Esse trecho é importante para colocar os IDs e Nomes para os Inputs -->
<?php foreach($dadosCompraValidar as $item) {?>
	<div class="modal fade" data-backdrop="static" id="ModeloValidacao<?=$item['idCompra']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-ms">
			<div class="modal-content">
				<form role="form" name="modalForm" id="modalFormValidacao<?=$item['idCompra']?>" action="<?=$textoDirecionar?>" method="post">
					<div class="modal-header bg-primary">
						<button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true" class="">×   </span><span class="sr-only">Fechar</span>

						</button>
						<h4 class="modal-title" id="myModalLabel">Validando Doação [<?=$item['nome']?> - <?=$item['nomeProduto']?>]</h4>
					</div>
					<div class="modal-body">
                        <div class="form-group">		
                            <input type="hidden" name="tipoAcao" id="tipoAcao" value="I" />
                            <input type="hidden" name="tipoPesquisa"  value="<?=$tipoPesquisa?>" />
                            <input type="hidden" name="textoPesquisa"  value="<?=$textoPesquisa?>" />
                            <input type="hidden" name="idCompra"  value="<?=$item['idCompra']?>" />
                        
                            <fieldset>
                                <legend>Dados para validação da doação</legend>                                
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label for="textoObservacao">Observação (Opicional) | Máximo de 500 caracteres </label>
                                        <textarea class="form-control"  name="textoObservacao" id="textoObservacao" rows="10" maxlength="500"></textarea>                              
                                    </div>                                    
                                </div>                                      
                            </fieldset>
                        </div>
                    </div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
						<button type="button" class="btn btn-success" autofocus  onclick="return salvarEnviarForm('modalFormValidacao<?=$item['idCompra']?>')"  >Validar Doação</button>
					</div>
				</form>
			</div>
		</div>
	</div>

<?php  }?>

<script>
    function salvarEnviarForm(form){              
        document.getElementById(form).submit();           
    }
</script>
