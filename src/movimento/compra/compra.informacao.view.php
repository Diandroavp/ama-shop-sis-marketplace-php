<!-- Esse trecho é importante para colocar os IDs e Nomes para os Inputs -->
<?php foreach($dadosCompra as $item) {?>
	<div class="modal fade" data-backdrop="static" id="ModeloCompra<?=$item['idCompra']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-ms">
			<div class="modal-content">
                <form role="form" name="modalForm" id="modalFormCompra<?=$item['idCompra']?>" action="<?=$textoDirecionar?>" method="post">
					<div class="modal-header bg-primary">
						<button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true" class="">×   </span><span class="sr-only">Fechar</span>

						</button>
						<h4 class="modal-title" id="myModalLabel">Informação da Doação [<?=$item['nome']?> - <?=$item['nomeProduto']?>]</h4>
					</div>
					<div class="modal-body">
                        <div class="form-group">		
                            <input type="hidden" name="tipoAcao" id="tipoAcao" value="EX" />
                            <input type="hidden" name="tipoPesquisa"  value="<?=$tipoPesquisa?>" />
                            <input type="hidden" name="textoPesquisa"  value="<?=$textoPesquisa?>" />
                            <input type="hidden" name="idCompra"  value="<?=$item['idCompra']?>" />

                            <input type="hidden" name="dataInicio"  value="<?=$dataInicio?>" />
                            <input type="hidden" name="dataFinal"  value="<?=$dataFinal?>" />
                            <input type="hidden" name="idOperador"  value="<?=$idOperador?>" />
                        
                            <fieldset>
                                <legend>Dados da Doação</legend>                                
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label for="textoObservacao">Usuário </label>
                                        <div class="form-group">
                                            <?=$item['nome']?>                                    
                                        </div>
                                    </div> 
                                    <div class="col-sm-4">
                                        <label for="textoObservacao">Usuário </label>
                                        <div class="form-group">
                                            <?=$item['cpf']?>                                    
                                        </div>
                                    </div>  
                                    <div class="col-sm-4">
                                        <label for="textoObservacao">Data da Doação </label>
                                        <div class="form-group">
                                            <?=date_dd_mm_yyyy($item['dataCompra'])?>                                       
                                        </div>                                                                      
                                    </div>                                    
                                </div>  
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label for="textoObservacao">Parceiro </label>
                                        <div class="form-group">
                                            <?=$item['nomeParceiro']?>                                    
                                        </div>
                                    </div> 
                                    <div class="col-sm-8">
                                        <label for="textoObservacao">Produto </label>
                                        <div class="form-group">
                                            <?=$item['nomeProduto']?>                                    
                                        </div>
                                    </div>                                      
                                </div>                                      
                            </fieldset>

                            <fieldset>
                                <legend>Dados da Validação da doação</legend>                                
                                <div class="row">
                                    <div class="col-sm-8">
                                        <label for="textoObservacao">Operador que validou a doação </label>
                                        <div class="form-group">
                                            <?=$item['nomeOperadorValidacao']?>                                    
                                        </div>
                                    </div>  
                                    <div class="col-sm-4">
                                        <label for="textoObservacao">Data da Validação </label>
                                        <div class="form-group">
                                            <?=date_dd_mm_yyyy($item['dataValidacao'])?>                                       
                                        </div>
                                                                      
                                    </div>                                    
                                </div>  
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label for="textoObservacao">Observação </label>
                                        <textarea class="form-control"  name="textoObservacao" id="textoObservacao" rows="5" maxlength="500" disabled><?=$item['textoObservacao']?></textarea>                              
                                    </div>                                    
                                </div>                                      
                            </fieldset>

                            

                            <fieldset>
                                <legend>Dados do Estorno</legend>                                
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label for="textoEstorno">Justificativa (Obrigatório) | Máximo de 500 caracteres </label>
                                        <textarea class="form-control"  name="textoEstorno" id="textoEstorno<?=$item['idCompra']?>" rows="5" maxlength="500" <?=(($item['dataEstorno']) ? 'disabled':'required')?> ><?=$item['textoEstorno']?></textarea>                              
                                    </div>                                    
                                </div>                                      
                            </fieldset>

                        </div>
                    </div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <?php if (!$item['dataEstorno']) {?>
                            <button type="button" class="btn btn-danger" autofocus  onclick="return salvarEnviarForm('modalFormCompra<?=$item['idCompra']?>', '<?=$item['idCompra']?>')"  >Estornar Compra</button>
                        <?}?>
					</div>
                </form>
			</div>
		</div>
	</div>

<?php  }?>
<script>
    function salvarEnviarForm(form, idCompra){  
        let textoEstorno = document.getElementById('textoEstorno' + idCompra);
        
        if ( textoEstorno.value.length > 15 ){
            if (confirm("Você confirma o estorno da doação: " + idCompra +"?")){
                 document.getElementById(form).submit();           
            }            
        } else {
            alert('informe uma justificativa válida! ( mais de 15 caracteres )');
            return false;
        }
        
    }
</script>
