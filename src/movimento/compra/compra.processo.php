<?php	
	$textoDirecionar = "?_p=".$_REQUEST["_p"];	
	$idOperadorCorrente = $_SESSION["_SESSION_idOperador"];
	// Tipo de Pesquisa
	$tipoPesquisa = (isset($_REQUEST['tipoPesquisa'])) ? $_REQUEST['tipoPesquisa'] : null;
	$textoPesquisa = (isset($_REQUEST['textoPesquisa'])) ? $_REQUEST['textoPesquisa'] : null;
	// CRUD
	$tipoAcao = (isset($_REQUEST['tipoAcao'])) ? $_REQUEST['tipoAcao'] : 'S';
	$nomeUsuario = (isset($_REQUEST['nomeUsuario'])) ? $_REQUEST['nomeUsuario'] : null;  
	$numeroCelular = (isset($_REQUEST['numeroCelular'])) ? $_REQUEST['numeroCelular'] : null;  
	$numeroCPF = (isset($_REQUEST['numeroCPF'])) ? $_REQUEST['numeroCPF'] : null;  
	$idCompra = (isset($_REQUEST['idCompra'])) ? $_REQUEST['idCompra'] : null;  	
	$idOperador = (isset($_REQUEST['idOperador']) && ($_REQUEST['idOperador'] <> '')) ? $_REQUEST['idOperador'] : null;  	
	$dataInicio = (isset($_REQUEST['dataInicio'])) ? $_REQUEST['dataInicio'] : date('Y-m-d');  	
	$dataFinal = (isset($_REQUEST['dataFinal'])) ? $_REQUEST['dataFinal'] : date('Y-m-d');  	
	$textoObservacao = (isset($_REQUEST['textoObservacao'])) ? $_REQUEST['textoObservacao'] : null;   	
	$textoEstorno = (isset($_REQUEST['textoEstorno'])) ? $_REQUEST['textoEstorno'] : null;   
	$idOperadorEstorno = $idOperadorCorrente;
	

	
	// Retorno de Resposta
	$tipoResposta = (isset($_REQUEST['tipoResposta'])) ? $_REQUEST['tipoResposta'] : null;
	$textoRetorno = (isset($_REQUEST['textoRetorno'])) ? $_REQUEST['textoRetorno'] : null;
	$ativoRetorno = (isset($_REQUEST['ativoRetorno'])) ? $_REQUEST['ativoRetorno'] : null;	


    if (isset($_REQUEST['tipoAcao']) && ($tipoAcao != 'S')){
		
		$dadosResposta = compra($tipoAcao, 
								$nomeUsuario, 
								$numeroCelular, 
								$numeroCPF, 
								$idCompra, 
								$idOperador, 
								convertDataParaBanco($dataInicio),
								convertDataParaBanco($dataFinal),
								$ativoProducao,
								$idOperadorCorrente,
								$textoEstorno);
								
		if ($dadosResposta and ($dadosResposta[0]['ativoRetorno'])){
			//AUDITORIA
			include "compra.auditoria.php";
		}
		//
		include "compra.processo.resposta.php";
    }
	if (isset($_REQUEST['tipoResposta'])){
		showMessage(($ativoRetorno == 1) ? "S" : "E", $_CONFIGURACAO_TITULO." - Informativo", $textoRetorno);
	}	

	// Montando tipo de Pesquisa
	$paramPesquisa = array();
	$paramPesquisa[] = array('numeroCelular', 'Celular');
	$paramPesquisa[] = array('nomeUsuario', 'Usuário');
	$paramPesquisa[] = array('numeroCPF', 'CPF');

	// 
	if (($tipoAcao == 'S')) {
		// Dados da grid principal
		$dadosCompra = compra( $tipoAcao, 
									(($textoPesquisa != '') && ($tipoPesquisa == 'nomeUsuario')) ? $textoPesquisa : null, 
									(($textoPesquisa != '') && ($tipoPesquisa == 'numeroCelular')) ? $textoPesquisa : null, 
									(($textoPesquisa != '') && ($tipoPesquisa == 'numeroCPF')) ? $textoPesquisa : null,
									$idCompra, 
									$idOperador, 
									convertDataParaBanco($dataInicio),
									convertDataParaBanco($dataFinal),
									$ativoProducao,
									$idOperadorEstorno,
									$textoEstorno);
	}
		
	$dadosOperador = operador('s', null,null, null,null, null,null);

    $numeroLinhas = 0;
?>

