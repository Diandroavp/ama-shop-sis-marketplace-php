<div class="container " style="overflow-y: auto; min-height:450px; ">
    <div class="page-header">
        <h4>Movimento/Histórico de Doações</h4>      
    </div>
	<div class="well">
        <form role="form" name="modalForm" action="<?=$textoDirecionar?>" method="post" autocomplete="off">
            <input type="hidden" name="tipoAcao" id="tipoAcao" value="S" />
            <div class="row">				
                <div class="col-sm-6">
                    <label for="dataInicio">Data Inicial</label>
					<div class="form-group">
                        <input type="date" class="form-control" name="dataInicio" id="dataInicio" value="<?=$dataInicio?>" />  
					</div>
                </div>					
                <div class="col-sm-6">
                    <label for="dataFinal">Data Final</label>
					<div class="form-group">
                        <input type="date" class="form-control" name="dataFinal" id="dataFinal" value="<?=$dataFinal?>" />  
					</div>
                </div>					
            </div>
            <div class="row">				
                <div class="col-sm-4">
                    <label for="Nome">Operador</label>
					<div class="form-group">
					  <select class="form-control" id="idOperador" name="idOperador" >							
                        <option <?=(($idOperador == null) ? 'selected':'')?>></option>
						<?php foreach($dadosOperador as $item) {?>
						  <option value="<?=$item['idOperador']?>" <?=(($item['idOperador'] == $idOperador) ? 'selected':'')?>><?=$item['nomeOperador']?></option>
						<?php }?>  
					  </select>
					</div>
                </div>		
                <div class="col-sm-2">
                    <label for="Nome">Tipo de Pesquisa</label>
					<div class="form-group">
					  <select class="form-control" id="tipoPesquisa" name="tipoPesquisa" required>							
						<?php foreach($paramPesquisa as $item) {?>
						  <option value="<?=$item[0]?>" <?=(($item[0] == $tipoPesquisa) ? 'selected':'')?>><?=$item[1]?></option>
						<?php }?>  
					  </select>
					</div>
                </div>					
				<div class="col-sm-4">
                    <label for="Nome">Texto para pesquisa</label>
					<div class="form-group">
						<input class="form-control" name="textoPesquisa" id="textoPesquisa" value="<?=$textoPesquisa?>" />  
					</div>
                </div>				
				<div class="col-sm-2">
					<label for="Nome"> &nbsp; </label>
					<div class="form-group text-right">
						<button type="submit" class="btn btn-primary"  > Localizar </button>
					</div>						
				</div>  							
            </div>
        </form>		
    </div>
	
    <div class="panel panel-default">
        <div class="panel-heading ">
			<strong>Listagem das Doações</strong>
        </div>
		<?php if($dadosCompra) {?>
			<input class="form-control" id="buscarModelo" type="text" placeholder="Buscar..">
		<?php }?>	
        <div class="table-responsive" style="overflow-y: auto; max-height:350px;">
            <table class="table table-hover table-striped sortable" id="tableModelo" >
                <thead>
                    <tr>
                        <th><strong>Data Doação</strong></th>
                        <th><strong>Celular</strong></th>
                        <th><strong>Usuário</strong></th>
                        <th><strong>CPF</strong></th>                        
                        <th><strong>Data Estorno</strong></th>
                        <th><strong>Operador Estorno</strong></th>
                        <th><strong></strong></th>						
                    </tr>
                </thead>                
                <tbody>
                    <?php foreach($dadosCompra as $item) {?>
                    <tr>
                        <td><?=date_dd_mm_yyyy_hh_mm_ss($item['dataCompra'])?></td>                        
                        <td><?=formatarTelefone($item['celular'])?></td>
                        <td><?=$item['nome']?></td>
                        <td><?=formatarCPForCNPJ($item['cpf'])?></td>                        
                        <td><?=date_dd_mm_yyyy_hh_mm_ss($item['dataEstorno'])?></td>
                        <td><?=$item['nomeOperadorEstorno']?></td>                        
                        <td>
                            <button class="btn <?=((!$item['dataEstorno']) ? 'btn-primary' : 'btn-danger')?>" data-toggle="modal" data-target="#ModeloCompra<?=$item['idCompra']?>" title="Informações da Doação" >Informações</button>                                                                                                            
                        </td>
                    </tr>
                    <?php $numeroLinhas++; }?>
                </tbody>
            </table>
        </div>
        <br>
        <div class="panel-footer">
            <?=$numeroLinhas." Registros encontrados..."?>
			<label for="Nome"> &nbsp; </label>
			<div class="form-group text-right">
				
			</div>	
        </div>
    </div>    
</div>
<?php
    include "compra.informacao.view.php";
?>

<script>
	$(document).ready(function(){
	  $("#buscarModelo").on("keyup", function() {
		var value = $(this).val().toLowerCase();
		$("#tableModelo tr").filter(function() {
		  $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
		});
	  });
	});
</script>   

