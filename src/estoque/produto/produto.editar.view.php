<!-- Esse trecho é importante para colocar os IDs e Nomes para os Inputs -->
<?php foreach($dadosProduto as $item) {?>
	<div class="modal fade" data-backdrop="static" id="ModeloEditar<?=$item['idProduto']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form role="form" name="modalForm" id="modalFormEditar<?=$item['idProduto']?>" action="<?=$textoDirecionar?>" method="post">
					<div class="modal-header bg-primary">
						<button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true" class="">×   </span><span class="sr-only">Fechar</span>

						</button>
						<h4 class="modal-title" id="myModalLabel">Editando Registro [<?=$item['idProduto']?> - <?=$item['nomeProduto']?>]</h4>
					</div>
					<div class="modal-body">
                        <div class="form-group">                            		
                            <input type="hidden" name="tipoAcao" id="tipoAcao" value="E" />
                            <input type="hidden" name="tipoPesquisa"  value="<?=$tipoPesquisa?>" />
                            <input type="hidden" name="textoPesquisa"  value="<?=$textoPesquisa?>" />                            
                            <input type="hidden" name="idProduto"  value="<?=$item['idProduto']?>" />

                            <fieldset>
                                <legend>Dados do Produto</legend>
                                <div class="row">
                                    <div class="col-sm-2">
                                        <label for="Código">Código</label>
                                        <input class="form-control" Disabled  value="<?=$item['idProduto']?>" />                               
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="nomeProduto">Nome do Produto</label>
                                        <input class="form-control"  name="nomeProduto" id="nomeProduto" value="<?=$item['nomeProduto']?>" required/>                               
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="valorProduto">Valor do Produto</label>
                                        <input class="form-control"  name="valorProduto" id="valorProduto" value="<?=$item['valorProduto']?>" required/>                               
                                    </div>              
                                    <div class="col-sm-3">
                                        <label for="valorOriginal">Valor Original</label>
                                        <input class="form-control"  name="valorOriginal" id="valorOriginal" value="<?=$item['valorOriginal']?>" required/>                               
                                    </div>                                	                                
                                </div>
                                <div class="row">                                    
                                    <div class="col-sm-3">
                                        <label for="percentualDesconto">Percentual Desconto</label>
                                        <input class="form-control"  name="percentualDesconto" id="percentualDesconto" value="<?=$item['percentualDesconto']?>" required/>                               
                                    </div>                                	                                
                                </div>
                                
                                <div class="row">
                                    <div class="col-sm-4">                                    
                                        <label>Mostrar na Home <input class="form-control" type="checkbox"  name="ativoMostrarTelaPrincipal" id="ativoMostrarTelaPrincipal" value="1"  <?=($item['ativoMostrarTelaPrincipal'] ? 'checked' : '' )?> /></label>
                                    </div>
                                    <div class="col-sm-4">                                    
                                        <label>Controlar por Código <input class="form-control" type="checkbox"  name="ativoControleCodigo" id="ativoControleCodigo" value="1"  <?=($item['ativoControleCodigo'] ? 'checked' : '' )?> /></label>
                                    </div>
                                    <div class="col-sm-3">                                    
                                        <label>Especial Natal <input class="form-control" type="checkbox"  name="ativoEventoNatal" id="ativoEventoNatal" value="1"  <?=($item['ativoEventoNatal'] ? 'checked' : '' )?> /></label>
                                    </div>  
                                    <div class="col-sm-4">                                    
                                        <label>Produto Ativo <input class="form-control" type="checkbox"  name="ativoProduto" id="ativoProduto" value="1"  <?=($item['ativoProduto'] ? 'checked' : '' )?> /></label>
                                    </div>               
                                </div>    
                            </fieldset>     

                            <fieldset>
                                <legend>Parceiro e Estoque</legend>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label for="idParceiro">Parceiro</label>
                                        <div class="form-group">
                                            <select class="form-control" id="idParceiro" name="idParceiro" >                     
                                                <?php foreach($dadosParceiros as $itemP) {?>
                                                <option value="<?=$itemP['idParceiro']?>" <?=(($itemP['idParceiro'] == $item['idParceiro']) ? 'selected':'')?>><?=$itemP['nomeParceiro']?></option>
                                                <?php }?>  
                                            </select>
                                        </div>
                                                                  
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="quantidadeDisponivel">Quantidade Para Estoque</label>
                                        <input class="form-control"  name="quantidadeDisponivel" id="quantidadeDisponivel" value="<?=$item['quantidadeDisponivel']?>" required/>                               
                                    </div>              
                                    <div class="col-sm-4">
                                        <label for="quantidadeQuilograma">Quantidade Em Quilogramas</label>
                                        <input class="form-control"  name="quantidadeQuilograma" id="quantidadeQuilograma" value="<?=$item['quantidadeQuilograma']?>" />                               
                                    </div>                                	                                
                                </div>
                                   
                            </fieldset>  

                            <fieldset>
                                <legend>Informações Adicionais</legend>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="nomeImagem">Imagem Home</label>
                                        <input class="form-control"  name="nomeImagem" id="nomeImagem" value="<?=$item['nomeImagem']?>" required/>                               
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="nomeImagemSecundaria">Imagem Home Secundária</label>
                                        <input class="form-control"  name="nomeImagemSecundaria" id="nomeImagemSecundaria" value="<?=$item['nomeImagemSecundaria']?>" required/>                               
                                    </div>                                                                 	                                
                                </div>

                                

                                <div class="row">
                                    <div class="col-sm-4">
                                        <label for="tipoProduto">Tipo Produto</label>
                                        <div class="form-group">
                                            <select class="form-control" id="tipoProduto" name="tipoProduto" >                     
                                                <?php foreach($paramTipoProduto as $itemP) {?>
                                                <option value="<?=$itemP['tipoProduto']?>" <?=(($itemP['tipoProduto'] == $item['tipoProduto']) ? 'selected':'')?>><?=$itemP['nomeTipoProduto']?></option>
                                                <?php }?>  
                                            </select>
                                        </div>                                      
                                    </div> 
                                    <div class="col-sm-4">
                                        <label for="tipoGiftCard">Tipo GiftCard ( Caso o tipo de Produto seja GiftCard )</label>
                                        <select class="form-control" id="tipoGiftCard" name="tipoGiftCard" >                     
                                            <?php foreach($paramTipoGiftCard as $itemP) {?>
                                            <option value="<?=$itemP['tipoGiftCard']?>" <?=(($itemP['tipoGiftCard'] == $item['tipoGiftCard']) ? 'selected':'')?>><?=$itemP['nomeTipoGiftCard']?></option>
                                            <?php }?>  
                                        </select>                                        
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="nomeOperadoraRecarga">Tipo Operadora ( Caso o tipo de Produto seja Recarga )</label>
                                        <select class="form-control" id="nomeOperadoraRecarga" name="nomeOperadoraRecarga" >                     
                                            <?php foreach($paramTipoOperadora as $itemP) {?>
                                            <option value="<?=$itemP['nomeOperadoraRecarga']?>" <?=(($itemP['nomeOperadoraRecarga'] == $item['nomeOperadoraRecarga']) ? 'selected':'')?>><?=$itemP['nomeOperadoraRecarga']?></option>
                                            <?php }?>  
                                        </select>                                        
                                    </div>                               	                                
                                </div>
                                   
                            </fieldset> 
                            
                            <fieldset>
                                <legend>Descrições e Instruções</legend>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label for="textoDescricao">Descrição</label>
                                        <textarea class="form-control"  name="textoDescricao" id="textoDescricao" rows="10"><?=$item['textoDescricao']?></textarea>                              
                                    </div>                                                 	                                
                                </div> 

                                <div class="row">
                                    <div class="col-sm-12">
                                        <label for="textoInstrucao">Instrução</label>
                                        <textarea class="form-control"  name="textoInstrucao" id="textoInstrucao" rows="10"><?=$item['textoInstrucao']?></textarea>                               
                                    </div>                                                        	                                
                                </div>                                   
                            </fieldset> 

                        </div>
                    </div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
						<button type="button" class="btn btn-success"  onclick="return salvarEnviarForm('modalFormEditar<?=$item['idProduto']?>')"  >Salvar Mudanças</button>
					</div>
				</form>
			</div>
		</div>
	</div>

<?php  }?>

<script>
    function salvarEnviarForm(form){              
        document.getElementById(form).submit();           
    }
</script>
