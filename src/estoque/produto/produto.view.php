<div class="container " style="overflow-y: auto; min-height:450px;">
    <div class="page-header">
        <h4>Estoque/Produtos</h4>      
    </div>
	<div class="well">
        <form role="form" name="modalForm" action="<?=$textoDirecionar?>" method="post" autocomplete="off">
        <input type="hidden" name="tipoAcao" id="tipoAcao" value="S" />
            <div class="row">
                <div class="col-sm-2">
                    <label for="Nome">Tipo de Pesquisa</label>
					<div class="form-group">
					  <select class="form-control" id="tipoPesquisa" name="tipoPesquisa" required>							
						<?php foreach($paramPesquisa as $item) {?>
						  <option value="<?=$item[0]?>" <?=(($item[0] == $tipoPesquisa) ? 'selected':'')?>><?=$item[1]?></option>
						<?php }?>  
					  </select>
					</div>
                </div>					
				<div class="col-sm-6">
                    <label for="Nome">Texto para pesquisa</label>
					<div class="form-group">
						<input class="form-control" name="textoPesquisa" id="textoPesquisa" value="<?=$textoPesquisa?>" />  
					</div>
                </div>	
                <div class="col-sm-2">
                    <label for="Nome">Produtos</label>
					<div class="form-group">                    
					<select class="form-control" id="tipoProdutoPesquisa" name="tipoProdutoPesquisa" >                                             
                        <option value="1" <?=(($tipoProdutoPesquisa == 1) ? 'selected':'')?>> Ativos </option>
                        <option value="2" <?=(($tipoProdutoPesquisa == 2) ? 'selected':'')?>> Inativos </option>
                        <option value="3" <?=(($tipoProdutoPesquisa == 3) ? 'selected':'')?>> Todos </option>    
                    </select>
                    
                    </div>
                </div>				
				<div class="col-sm-2">
					<label for="Nome"> &nbsp; </label>
					<div class="form-group text-right">
						<button type="submit" class="btn btn-primary" > Localizar </button>
					</div>						
				</div>  							
            </div>
        </form>		
    </div>
    <div class="panel panel-default">
        <div class="panel-heading ">
			<strong>Listagem dos Produtos</strong>
        </div>
		<?php if($dadosProduto) {?>
			<input class="form-control" id="buscarModelo" type="text" placeholder="Buscar..">
		<?php }?>	
        <div class="table-responsive" style="overflow-y: auto; max-height:350px;">
            <table class="table table-hover table-striped sortable" id="tableModelo" >
                <thead>
                    <tr>
                        <th><strong>ID</strong></th>
                        <th><strong>Nome</strong></th>
                        <th><strong>Valor (R$)</strong></th>
                        <th><strong>Orignial (R$)</strong></th>
                        <th><strong>Percentual (%)</strong></th>                        
                        <th><strong>Estoque</strong></th>
                        <th><strong>Parceiro</strong></th>
                        <th><strong>Tipo</strong></th>
                        <th><strong>Ativo</strong></th>
                        <th><strong>Home</strong></th>
                        <th><strong></strong></th>						
                    </tr>
                </thead>                
                <tbody>
                    <?php foreach($dadosProduto as $item) {
                        $textoAviso = '';
                        //
                        if (!$item['ativoProduto']){
                            $textoAviso = $textoAviso.'- Produto Desativado!';	
                        }?>
                    <tr class=" <?=(!$item['ativoProduto']) ? 'danger' : '' ?>" title="<?=$textoAviso?>">
                        <td><?=$item['idProduto']?></td>
                        <td><?=$item['nomeProduto']?></td>
                        <td><?=formatar_moeda($item['valorProduto'],2)?></td>
                        <td><?=formatar_moeda($item['valorOriginal'],2)?></td>
                        <td><?=formatar_percentual($item['percentualDesconto'],2)?></td>
                        <td><?=$item['quantidadeDisponivel']?></td>                        
                        <td><?=$item['nomeParceiro']?></td>
                        <td><?=$item['tipoProduto']?></td>
                        <td><?=simOuNao($item['ativoProduto'])?></td>
                        <td><?=simOuNao($item['ativoMostrarTelaPrincipal'])?></td>
                        <td>                            
                            <button class="btn btn-primary" data-toggle="modal" data-target="#ModeloEditar<?=$item['idProduto']?>" title="Editar Registro" >Editar</button>
                            <button type="button" class="btn btn-danger" onclick="enviaform('Deseja excluir o registro <?=$item['idProduto']?> - <?=$item['nomeProduto']?>?', '<?=$item['idProduto']?>')"  title="Excluir Registro">Excluir</button>                                                        
                            <button type="button" class="btn btn-primary" onclick="enviaFormGeral('formImagems<?=$item['idProduto']?>')"  title="Acessar as imagens">Imagens</button>
                            <!--<button class="btn btn-primary" data-toggle="modal" data-target="#ModeloGerarParcelas<?=$item['idProduto']?>" title="Gerar Parcelas para o Contrato" >Categorias</button>-->
                        </td>
                    </tr>
                    <?php $numeroLinhas++; }?>
                </tbody>
            </table>
        </div>
        <br>
        <div class="panel-footer">
            <?=$numeroLinhas." Registros encontrados..."?>
			<label for="Nome"> &nbsp; </label>
			<div class="form-group text-right">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalAdicionar">+ Novo Contrato</button>						                
			</div>	
        </div>
    </div>    
</div>
<?php
	include "produto.editar.view.php";
    include "produto.adicionar.view.php";
    include "produto.excluir.view.php";
?>

<script>
	$(document).ready(function(){
	  $("#buscarModelo").on("keyup", function() {
		var value = $(this).val().toLowerCase();
		$("#tableModelo tr").filter(function() {
		  $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
		});
	  });
	});
</script>   

<?php foreach($dadosProduto as $item) {?>
    <form role="form"  id="formImagems<?=$item['idProduto']?>" action="?_p=pimg" method="post">					                           		
        <input type="hidden" name="tipoPesquisa"  value="<?=$tipoPesquisa?>" />
        <input type="hidden" name="textoPesquisa"  value="<?=$textoPesquisa?>" />
        <input type="hidden" name="idProduto"  value="<?=$item['idProduto']?>" />
        <input type="hidden" name="nomeProduto"  value="<?=$item['nomeProduto']?>" />
    </form>
<?}?>
