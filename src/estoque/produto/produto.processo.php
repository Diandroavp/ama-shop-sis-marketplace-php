<?php	
    $textoDirecionar = "?_p=".$_REQUEST["_p"];	
	$idOperadorCorrente = $_SESSION["_SESSION_idOperador"];

	// Tipo de Pesquisa
	$tipoPesquisa = (isset($_REQUEST['tipoPesquisa'])) ? $_REQUEST['tipoPesquisa'] : null;
	$textoPesquisa = (isset($_REQUEST['textoPesquisa'])) ? $_REQUEST['textoPesquisa'] : null;
	
	// CRUD
	$tipoAcao = (isset($_REQUEST['tipoAcao'])) ? $_REQUEST['tipoAcao'] : 'S';
	$idProduto = (isset($_REQUEST['idProduto'])) ? $_REQUEST['idProduto'] : null;  
	$nomeProduto = (isset($_REQUEST['nomeProduto'])) ? $_REQUEST['nomeProduto'] : null;  
	$tipoProdutoPesquisa = (isset($_REQUEST['tipoProdutoPesquisa'])) ? $_REQUEST['tipoProdutoPesquisa'] : 1;  
	$valorProduto = (isset($_REQUEST['valorProduto'])) ? str_replace(',', '.', $_REQUEST['valorProduto']) : null;  
	$valorOriginal = (isset($_REQUEST['valorOriginal'])) ? str_replace(',', '.', $_REQUEST['valorOriginal']) : null;  
	$nomeImagem = (isset($_REQUEST['nomeImagem'])) ? $_REQUEST['nomeImagem'] : null;  
	$nomeImagemSecundaria = (isset($_REQUEST['nomeImagemSecundaria'])) ? $_REQUEST['nomeImagemSecundaria'] : null;  	
	$percentualDesconto = (isset($_REQUEST['percentualDesconto'])) ? $_REQUEST['percentualDesconto'] : null;  
	$idParceiro = (isset($_REQUEST['idParceiro'])) ? $_REQUEST['idParceiro'] : null;  	
	$textoDescricao = (isset($_REQUEST['textoDescricao'])) ? str_replace("'", "", $_REQUEST['textoDescricao']) : null;  
	$ativoMostrarTelaPrincipal = (isset($_REQUEST['ativoMostrarTelaPrincipal'])) ? $_REQUEST['ativoMostrarTelaPrincipal'] : null;  
	$tipoProduto = (isset($_REQUEST['tipoProduto'])) ? $_REQUEST['tipoProduto'] : null;  
	$ativoProduto = (isset($_REQUEST['ativoProduto'])) ? $_REQUEST['ativoProduto'] : null;  
	$quantidadeDisponivel = (isset($_REQUEST['quantidadeDisponivel'])) ? $_REQUEST['quantidadeDisponivel'] : null;  
	$tipoGiftCard = (isset($_REQUEST['tipoGiftCard'])) ? $_REQUEST['tipoGiftCard'] : null;  
	$nomeOperadoraRecarga = (isset($_REQUEST['nomeOperadoraRecarga'])) ? $_REQUEST['nomeOperadoraRecarga'] : null;  
	$ativoControleCodigo = (isset($_REQUEST['ativoControleCodigo'])) ? $_REQUEST['ativoControleCodigo'] : null;  
	$textoInstrucao = (isset($_REQUEST['textoInstrucao'])) ? str_replace("'", "", $_REQUEST['textoInstrucao']) : null;  
	$ativoEventoNatal = (isset($_REQUEST['ativoEventoNatal'])) ? $_REQUEST['ativoEventoNatal'] : 0;  
	$quantidadeQuilograma = (isset($_REQUEST['quantidadeQuilograma'])) ? $_REQUEST['quantidadeQuilograma'] : null;  
	
	
	// Retorno de Resposta
	$tipoResposta = (isset($_REQUEST['tipoResposta'])) ? $_REQUEST['tipoResposta'] : null;
	$textoRetorno = (isset($_REQUEST['textoRetorno'])) ? $_REQUEST['textoRetorno'] : null;
	$ativoRetorno = (isset($_REQUEST['ativoRetorno'])) ? $_REQUEST['ativoRetorno'] : null;	
	 

    if (isset($_REQUEST['tipoAcao']) && ($tipoAcao != 'S')){
		
		$dadosResposta = produto($tipoAcao, $idProduto, $nomeProduto, $tipoProdutoPesquisa, $valorProduto, $valorOriginal, $nomeImagem, $nomeImagemSecundaria, $percentualDesconto, $idParceiro, $textoDescricao, $ativoMostrarTelaPrincipal, $tipoProduto, $ativoProduto, $quantidadeDisponivel, $tipoGiftCard, $nomeOperadoraRecarga, $ativoControleCodigo, $textoInstrucao, $ativoEventoNatal, $quantidadeQuilograma);
		
		if ($dadosResposta and ($dadosResposta[0]['ativoRetorno'])){
			//AUDITORIA
			Include "produto.auditoria.php";	
		}			
		//
		include "produto.processo.resposta.php";
    }
	if (isset($_REQUEST['tipoResposta'])){
		showMessage(($ativoRetorno == 1) ? "S" : "E", $_CONFIGURACAO_TITULO." - Informativo", $textoRetorno);
	}	

	// Montando tipo de Pesquisa
	$paramPesquisa = array();
	$paramPesquisa[] = array('ID', 'ID');
	$paramPesquisa[] = array('Nome', 'Nome');

	// Montando Tipo Produto
	$paramTipoProduto = array();
	$paramTipoProduto[] = array('tipoProduto'=> 'p', 'nomeTipoProduto'	=> 'Produto');
	$paramTipoProduto[] = array('tipoProduto'=> 's', 'nomeTipoProduto'	=> 'Serviço');
	$paramTipoProduto[] = array('tipoProduto'=> 'vc', 'nomeTipoProduto'	=> 'Vale Compra');
	$paramTipoProduto[] = array('tipoProduto'=> 'g', 'nomeTipoProduto'	=> 'GiftCard');
	$paramTipoProduto[] = array('tipoProduto'=> 'r', 'nomeTipoProduto'	=> 'Recarga');
	
	// Montando Tipo GiftCard
	$paramTipoGiftCard = array();
	$paramTipoGiftCard[] = array('tipoGiftCard'=> '', 'nomeTipoGiftCard'	=> '');
	$paramTipoGiftCard[] = array('tipoGiftCard'=> 'g', 'nomeTipoGiftCard'	=> 'Google Play');
	$paramTipoGiftCard[] = array('tipoGiftCard'=> 'n', 'nomeTipoGiftCard'	=> 'Netflix');
	$paramTipoGiftCard[] = array('tipoGiftCard'=> 's', 'nomeTipoGiftCard'	=> 'Spotify');
	$paramTipoGiftCard[] = array('tipoGiftCard'=> 'u', 'nomeTipoGiftCard'	=> 'Uber');	
	

	// Montando Tipo Recarga
	$paramTipoOperadora = array();
	$paramTipoOperadora[] = array('nomeOperadoraRecarga'	=> '');
	$paramTipoOperadora[] = array('nomeOperadoraRecarga'	=> 'CLARO');
	$paramTipoOperadora[] = array('nomeOperadoraRecarga'	=> 'OI');
	$paramTipoOperadora[] = array('nomeOperadoraRecarga'	=> 'TIM');
	$paramTipoOperadora[] = array('nomeOperadoraRecarga'	=> 'VIVO');
	
	
	

	
	// 
	$textoAviso = '';
	// Abrindo Parceiros para pesquisa
	$dadosParceiros = parceirosSelecionar(null);

	if (($tipoAcao == 'S')) {
		// Dados da grid principal
		$dadosProduto = produto(	$tipoAcao, 
									(($textoPesquisa != '') && ($tipoPesquisa == 'ID')) ? $textoPesquisa : null,
									(($textoPesquisa != '') && ($tipoPesquisa == 'Nome')) ? $textoPesquisa : null,
									$tipoProdutoPesquisa,
									$valorProduto, 
									$valorOriginal, 
									$nomeImagem, 
									$nomeImagemSecundaria,
									$percentualDesconto, 
									$idParceiro, 									 
									$textoDescricao,
									$ativoMostrarTelaPrincipal, 
									$tipoProduto, 
									$ativoProduto, 
									$quantidadeDisponivel, 
									$tipoGiftCard, 
									$nomeOperadoraRecarga, 
									$ativoControleCodigo, 
									$textoInstrucao,
									$ativoEventoNatal,
									$quantidadeQuilograma
								);				
	}

	$numeroLinhas = 0;
?>

