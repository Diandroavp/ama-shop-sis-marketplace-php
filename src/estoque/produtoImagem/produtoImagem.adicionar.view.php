<!-- Esse trecho é importante para colocar os IDs e Nomes para os Inputs -->
<div class="modal fade " data-backdrop="static" id="myModalAdicionar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content" >
            <form role="form" name="modalForm" id="modalFormAdicionar" action="<?=$textoDirecionar?>" method="post" autocomplete="off">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true" class="">×   </span><span class="sr-only">Fechar</span>

                    </button>
                    <h4 class="modal-title" id="myModalLabel">Novo Registro</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">		
                        <input type="hidden" name="tipoAcao" id="tipoAcao" value="I" />
                        <input type="hidden" name="tipoPesquisa"  value="<?=$tipoPesquisa?>" />
                        <input type="hidden" name="textoPesquisa"  value="<?=$textoPesquisa?>" />
                        <input type="hidden" name="nomeProduto"  value="<?=$nomeProduto?>" />
                        <input type="hidden" name="idProduto"  value="<?=$idProduto?>" />
                        
                        <fieldset>
                                <legend>Dados da Imagem</legend>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label for="Código">ID do Produto</label>
                                        <input class="form-control" Disabled  value="<?=$idProduto?>" />                               
                                    </div>
                                    <div class="col-sm-8">
                                        <label for="numeroParcela">Nome do Produto</label>
                                        <input class="form-control" Disabled value="<?=$nomeProduto?>" required/>
                                    </div>                            	                                
                                </div>        
                                <div class="row">      
                                    <div class="col-sm-12">
                                        <label for="nomeImagem">Nome da Imagem</label>
                                        <input class="form-control"  name="nomeImagem" id="nomeImagem"  required/>                               
                                    </div>                             	                                
                                </div>                                    
                            </fieldset>       
                   
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <button type="submit" class="btn btn-success" >Salvar Mudanças</button>
                </div>
            </form>
        </div>
    </div>
</div>