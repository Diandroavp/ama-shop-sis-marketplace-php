<!-- Esse trecho é importante para colocar os IDs e Nomes para os Inputs -->
<?php foreach($dadosProdutoImagem as $item) {?>
	<div class="modal fade" data-backdrop="static" id="ModeloEditar<?=$item['idImagem']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form role="form" name="modalForm" id="modalFormEditar<?=$item['idImagem']?>" action="<?=$textoDirecionar?>" method="post">
					<div class="modal-header bg-primary">
						<button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true" class="">×   </span><span class="sr-only">Fechar</span>

						</button>
						<h4 class="modal-title" id="myModalLabel">Editando Imagem [<?=$item['idImagem']?>]</h4>
					</div>
					<div class="modal-body">
                        <div class="form-group">                            		
                            <input type="hidden" name="tipoAcao" id="tipoAcao" value="E" />
                            <input type="hidden" name="tipoPesquisa"  value="<?=$tipoPesquisa?>" />
                            <input type="hidden" name="textoPesquisa"  value="<?=$textoPesquisa?>" />
                            <input type="hidden" name="idProduto"  value="<?=$idProduto?>" />
                            <input type="hidden" name="idImagem"  value="<?=$item['idImagem']?>" />
                            <input type="hidden" name="nomeProduto"  value="<?=$nomeProduto?>" />

                            <fieldset>
                                <legend>Dados da Imagem</legend>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label for="Código">ID do Produto</label>
                                        <input class="form-control" Disabled  value="<?=$idProduto?>" />                               
                                    </div>
                                    <div class="col-sm-8">
                                        <label for="numeroParcela">Nome do Produto</label>
                                        <input class="form-control" Disabled value="<?=$nomeProduto?>" required/>
                                    </div>                            	                                
                                </div>        
                                <div class="row">      
                                    <div class="col-sm-4">
                                        <label for="Código">ID</label>
                                        <input class="form-control" Disabled  value="<?=$item['idImagem']?>" />                               
                                    </div>                             
                                    <div class="col-sm-8">
                                        <label for="nomeImagem">Nome da Imagem</label>
                                        <input class="form-control"  name="nomeImagem" id="nomeImagem" value="<?=$item['nomeImagem']?>" required/>                               
                                    </div>                             	                                
                                </div>                                    
                            </fieldset>                            
                        </div>
                    </div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
						<button type="button" class="btn btn-success"  onclick="return salvarEnviarForm('modalFormEditar<?=$item['idImagem']?>')"  >Salvar Mudanças</button>
					</div>
				</form>
			</div>
		</div>
	</div>

<?php  }?>

<script>
    function salvarEnviarForm(form){              
        document.getElementById(form).submit();           
    }
</script>
