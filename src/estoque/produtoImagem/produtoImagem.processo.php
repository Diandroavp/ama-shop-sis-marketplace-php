<?php	
    $textoDirecionar = "?_p=".$_REQUEST["_p"];	
	$idOperadorCorrente = $_SESSION["_SESSION_idOperador"];

	// Tipo de Pesquisa Da Tela Anterior
	$tipoPesquisa = (isset($_REQUEST['tipoPesquisa'])) ? $_REQUEST['tipoPesquisa'] : null;
	$textoPesquisa = (isset($_REQUEST['textoPesquisa'])) ? $_REQUEST['textoPesquisa'] : null;
	$idProduto = (isset($_REQUEST['idProduto'])) ? $_REQUEST['idProduto'] : null; 
	$nomeProduto = (isset($_REQUEST['nomeProduto'])) ? $_REQUEST['nomeProduto'] : null; 

	// CRUD
	$tipoAcao = (isset($_REQUEST['tipoAcao'])) ? $_REQUEST['tipoAcao'] : 'S';
	$idImagem = (isset($_REQUEST['idImagem'])) ? $_REQUEST['idImagem'] : null;  
	$nomeImagem = (isset($_REQUEST['nomeImagem'])) ? $_REQUEST['nomeImagem'] : null;  
	
	// Retorno de Resposta
	$tipoResposta = (isset($_REQUEST['tipoResposta'])) ? $_REQUEST['tipoResposta'] : null;
	$textoRetorno = (isset($_REQUEST['textoRetorno'])) ? $_REQUEST['textoRetorno'] : null;
	$ativoRetorno = (isset($_REQUEST['ativoRetorno'])) ? $_REQUEST['ativoRetorno'] : null;	
	 

    if (isset($_REQUEST['tipoAcao']) && ($tipoAcao != 'S')){
		
		$dadosResposta = produtoImagem($tipoAcao, $idImagem, $idProduto, $nomeImagem);
		
		if ($dadosResposta and ($dadosResposta[0]['ativoRetorno'])){
			//AUDITORIA
			include "produtoImagem.auditoria.php";
		}
		//
		include "produtoImagem.processo.resposta.php";
    }
	if (isset($_REQUEST['tipoResposta'])){
		showMessage(($ativoRetorno == 1) ? "S" : "E", $_CONFIGURACAO_TITULO." - Informativo", $textoRetorno);
	}	


	// 
	$textoAviso = '';

	if (($tipoAcao == 'S')) {
		// Dados da grid principal
		$dadosProdutoImagem = produtoImagem($tipoAcao, $idImagem, $idProduto, $nomeImagem);	
	}
	
    $numeroLinhas = 0;
?>

