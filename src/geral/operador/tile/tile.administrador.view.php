<div class="row">				
        <div class="col-sm-6 hover-zoom">
            <center>
                <a title="Validar Doações" href="?_p=vali">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <label for="Nome">Validar</label>
                            <div class="form-group">                                    
                                <i class='fas fa-cash-register' style='font-size:64px;color:#33CB00'></i>                                    
                            </div>
                        </div>        
                    </div>
                </a>                
            </center>
        </div>					
        <div class="col-sm-6 hover-zoom">
            <center>
                <a title="Histórico de validação" href="?_p=hisv">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <label for="Nome">Histórico</label>
                            <div class="form-group">                                    
                                <i class='fas fa-receipt' style='font-size:64px;color:#33CB00'></i>                                    
                            </div>
                        </div>        
                    </div>
                </a>            
            </center>
        </div>															            
    </div>