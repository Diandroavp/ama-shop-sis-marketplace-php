<?php
    if (isset($_REQUEST['nomeOperador']) and isset($_REQUEST['nomeSenha'])) {
        $nomeOperador = $_REQUEST['nomeOperador'];
        $nomeSenha = $_REQUEST['nomeSenha'];
        
        $dadosLogin = login($nomeOperador, $nomeSenha);
     
        if ($dadosLogin){

            $nomeOperador = explode(" ", $dadosLogin[0]['nomeOperador']);
            $_SESSION["_SESSION_nomeOperador"]			 	= $nomeOperador[0];
            $_SESSION["_SESSION_idOperador"]				= $dadosLogin[0]['idOperador'];            
            $_SESSION["_SESSION_eMail"]						= $dadosLogin[0]['eMail'];			
            $_SESSION["_SESSION_tipoAcesso"] 				= $dadosLogin[0]['tipoNivel'];
 
            forcarLocate('?');
        } else {
            showMessage("E", $_CONFIGURACAO_TITULO." - Erro", "Operador ou senha não identificado!");
        }
        
    }
?>