<ul class="nav navbar-nav navbar-right ">
    <li class="dropdown <?=(($_p == 'vali') || ($_p == 'hisv')) ? 'active' : '' ?>">
        <a class="dropdown-toggle " data-toggle="dropdown" href="#">Movimento <span class="caret"></span></a>
        <ul class="dropdown-menu">
            <li class="<?=(($_p == 'vali')) ? 'active' : '' ?>">
                <a title="Validação de doações efetuadas." href="?_p=vali">
                    Validar Doação
                </a>
            </li>              
            <li class="<?=(($_p == 'hisv')) ? 'active' : '' ?>">
                <a title="Histórico de Validações" href="?_p=hisv">
                    Histórico de Validação
                </a>
            </li>
        </ul>
    </li>
    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class='fas fa-user-alt'></i> <?= $_SESSION["_SESSION_nomeOperador"]?><span class="caret"></span></a>
        <ul class="dropdown-menu">
            <li><a href="?_p=sai"><i class='fas fa-sign-in-alt'></i></span> Sair</a></li>
        </ul>
    </li>
</ul>
