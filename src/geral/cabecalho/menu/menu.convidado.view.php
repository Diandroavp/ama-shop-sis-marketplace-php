<ul class="nav navbar-nav navbar-right ">
    <li class="nav-item <?=(($_p == 'dash')) ? 'active' : '' ?>">
        <a class="nav-link" href="?_p=dash">
            Dashboard <span class="sr-only">(current)</span>
        </a>
    </li>       
    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class='fas fa-user-alt'></i> <?= $_SESSION["_SESSION_nomeOperador"]?><span class="caret"></span></a>
        <ul class="dropdown-menu">
            <li><a href="?_p=sai"><i class='fas fa-sign-in-alt'></i></span> Sair</a></li>
        </ul>
    </li>
</ul>
