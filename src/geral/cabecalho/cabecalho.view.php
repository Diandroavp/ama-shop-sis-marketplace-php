﻿<nav id="navTop" class="navbar navbar-inverse box-area-clientes " data-spy="affix" style="width:100%; top:0; ">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>                        
                </button>
                <a class="navbar-brand "  href="?">
                    <a href="?"  onclick="enviaFormularioSimples('frmHome')">              
                        <img src="./imagens/logo-branco.svg" alt="" style="width: 100%;  max-width: 100px" >              
                    </a>   
					<span class="text-white" style="font-size:20px"> | <?=$_CONFIGURACAO_NOME_PRINCIPAL?> </span>
				</a>         
            </div>
    
    <div class="collapse navbar-collapse" id="myNavbar" >          
          <?php
                $_p = (isset($_REQUEST['_p'])) ? $_REQUEST['_p'] : null;
                $tipoAcesso = (isset($_SESSION["_SESSION_tipoAcesso"])) ? $_SESSION["_SESSION_tipoAcesso"] : null;
                switch($tipoAcesso) {
                    case "b": 
                        include "menu/menu.basico.view.php";
                    break;

                    case "a": 
                        include "menu/menu.administrador.view.php";
                    break; 

                    case "e": 
                        include "menu/menu.estoque.view.php";
                    break; 
                    
                    case "s": 
                        include "menu/menu.supervisor.view.php";
                    break; 

                    case "c": 
                        include "menu/menu.convidado.view.php";
                    break; 

                    default:
                        include "menu/menu.inicial.view.php";                                          
                       
                        break;
                }

            ?> 
        
    </div>
    
</nav>
