<head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <!--<meta charset="utf-8">-->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title><?=$_CONFIGURACAO_TITULO?></title>

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/bootstrap.min.css" rel="stylesheet">
		<script type="text/javascript" src="js/jquery.min.js"></script>    
		<script type="text/javascript" src="js/bootstrap.min.js"></script>   

        <!--JSON-->
        <script src="js/jquery-3.2.1.min.js"></script>
	

		<!-- Include external CSS. -->
		<!-- Editor -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.css">
		
		<!-- Include external JS libs. -->
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/mode/xml/xml.min.js"></script>
	 
		
        <!--FaveIcon-->
		<link rel="shortcut icon" href="imagens/icon.ico" type="image/x-icon"/>
	 

        <!-- ICONES -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
                integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ"
                crossorigin="anonymous">
		
		<!--Fim Editor-->
        <script type="text/javascript" src="js/padrao.js"></script>  
        <script type="text/javascript" src="js/graficos.js"></script>            
        <script type="text/javascript" src="js/cookies.js"></script>            
        <link href="css/padrao.css" rel="stylesheet">
        
		
		<!--Ordenação das Colunas-->
		<script type="text/javascript" src="js/sorttable.js"></script>
		
		<link href="css/content.css" rel="stylesheet">
   
        <!--CONFIGURAÇÃO LOADING TROCADOS-->
        <script type="text/javascript"
                src="https://cdnjs.cloudflare.com/ajax/libs/bodymovin/4.13.0/bodymovin.min.js"></script>

        <!--Chart-->
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        
        
    
</head>
