<div class="modal fade" data-backdrop="static" id="modal" role="dialog">
    
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header bg-primary">
            <button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true" class="">×   </span><span class="sr-only">Fechar</span></button>
			      <h4 class="modal-title" id="myModalLabel"><?=$tituloMensagem?></h4>        
        </div>
        <div class="modal-body">
          <p class=" <?=$classMensagem?>"><?=$textMensagem?></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
        </div>
      </div>

    </div>	

</div>